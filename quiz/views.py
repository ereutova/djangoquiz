import datetime
from collections import Counter, defaultdict
from random import shuffle

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.db import IntegrityError
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.template import loader

from .models import Question, Level, Answer, Language, User, Test, TestQuestion, TestNumber, UserAnswer


def level(request):
    all_levels = Level.objects.all()
    all_languages = Language.objects.all()
    try:
        user = User.objects.get(pk=request.session['user'])
        user_group = user.groups.get().name
    except (KeyError, ObjectDoesNotExist):
        user = None
        user_group = None
    context = {
        'all_levels': all_levels,
        'all_languages': all_languages,
        'user': user,
        'user_group': user_group
    }
    return render(request, 'quiz/level.html', context)


def register(request):
    return render(request, 'quiz/register.html')


def register_user(request):
    try:
        user = User.objects.create_user(request.POST['uname'], request.POST['email'], request.POST['psw'])
        group = Group.objects.get(name='simple_user')
        user.groups.add(group)
        request.session['user'] = user.id
        return redirect(level)
    except IntegrityError:
        return render(request, 'quiz/register.html', {'error_message': True})


def test(request, language_id, level_id):
    try:
        user = User.objects.get(pk=request.session['user'])
    except (KeyError, ObjectDoesNotExist):
        user = User.objects.get(username='GUEST')
        request.session['user'] = user.id
        group = Group.objects.get(name='guest')
        user.groups.add(group)

    test = Test(user_id=user, date=datetime.datetime.now())
    test.save()
    random_questions = Question.objects.filter(level=level_id, language_id=language_id).order_by('?')[:3]
    questions = []

    for question in random_questions:
        q = {'title': question, 'answers': [], 'type': question.question_type}
        if q['type'] != 'tf':
            answers = Answer.objects.filter(question=question)
            for answer in answers:
                q['answers'].append(answer)
        test_question = TestQuestion(test_id=test, question_id=question)
        test_question.save()
        questions.append(q)

    context = {
        'test_questions': questions,
        'level_id': level_id,
        'test_id': test.id
    }

    return render(request, 'quiz/test.html', context)


def login(request):
    return render(request, 'quiz/login.html')


def parse(request):
    user = authenticate(username=request.POST['uname'], password=request.POST['psw'])
    if not user:
        log = 'Please, try again'
        return render(request, 'quiz/login.html', {'log': log})
    request.session['user'] = user.id
    return redirect(level)


def logout(request):
    request.session['user'] = None
    return redirect(login)


def user_result(request):
    result_numbers = defaultdict(list)
    user = User.objects.get(pk=request.session['user'])
    tests = Test.objects.filter(user_id=user)
    group = user.groups.get().name
    if group == 'admin':
        tests = Test.objects.all()
    elif group == 'guest':
        return render(request, 'quiz/auth.html')
    for test in tests:
        try:
            test_number = TestNumber.objects.get(test_id=test)
            user = test_number.test_id.user_id
            test_question = TestQuestion.objects.filter(test_id=test)[0]
            level_id = test_question.question_id.level.level_number
            result_numbers[level_id].append((test_number, user))
        except ObjectDoesNotExist:
            continue
    for v in result_numbers.values():
        v.reverse()
    context = {
        'result_numbers': dict(result_numbers),
        'user': user
    }
    return render(request, 'quiz/user_result.html', context)


def check_authorization(func):
    def wrapper(*args, **kwargs):
        requested_user = User.objects.get(pk=args[0].session['user'])
        user_of_test = Test.objects.get(id=kwargs['test_id']).user_id
        if requested_user != user_of_test and requested_user.groups.get().name != 'admin':
            return render(args[0], 'quiz/auth.html')
        return func(*args, **kwargs)

    return wrapper


@check_authorization
def result_test(request, level_id, test_id):
    results = []
    result_count = Counter()
    test_questions = TestQuestion.objects.filter(test_id=test_id).order_by('id')
    user_group = User.objects.get(pk=request.session['user']).groups.get()
    for test in test_questions:
        question_id = test.question_id.id
        question = Question.objects.get(id=question_id)
        question_type = question.question_type
        answers = Answer.objects.filter(question=question_id)
        q = {
            'question': question,
            'answers': answers,
            'type': question_type,
            'level_id': level_id
        }
        if question_type == 'tf':
            check_tf_question(request, question_id, q)
        elif question_type == 'ch':
            check_ch_question(request, question_id, q, test.test_id)
        elif question_type == 'rd':
            check_rd_question(request, question_id, q, test.test_id)
        if q['is_correct']:
            result_count['correct'] += 1
        else:
            result_count['wrong'] += 1
        results.append(q)
    get_test_number(test_id, result_count)
    return render(request, 'quiz/result_test.html', {'result': results, 'user_group': user_group.name})


def get_test_number(test_id, result_count):
    test = Test.objects.get(id=test_id)
    try:
        TestNumber.objects.get(test_id=test)
    except ObjectDoesNotExist:
        test_result = TestNumber(test_id=test, correct=result_count['correct'], wrong=result_count['wrong'])
        test_result.save()


def get_chosen_answers(request, question, test):
    chosen_answer = [user_a.user_answer for user_a in UserAnswer.objects.filter(question_id=question, test_id=test)]
    if not chosen_answer:
        user_answer = Answer.objects.filter(id=request.POST.get(str(question.id)))
        for u_ans in user_answer:
            choice = UserAnswer(question_id=question, user_answer=u_ans, test_id=test)
            choice.save()
            chosen_answer.append(choice.user_answer)
    return chosen_answer


def check_tf_question(request, q_id, result):
    is_correct = False
    chosen_answer = request.POST.get(str(result['question'].id))
    correct_answers = Answer.objects.filter(question=q_id, is_correct=True)
    for answer in correct_answers:
        if chosen_answer == answer.answer_text:
            is_correct = True
    result.update({
        'chosen_answer': chosen_answer,
        'correct_answer': correct_answers,
        'is_correct': is_correct
    })


def check_rd_question(request, q_id, result, test):
    chosen_answer = get_chosen_answers(request, result['question'], test)
    correct_answers = Answer.objects.filter(question=q_id, is_correct=True)
    is_correct = chosen_answer in correct_answers
    result.update({
        'chosen_answer': chosen_answer,
        'correct_answer': correct_answers,
        'is_correct': is_correct
    })


def check_ch_question(request, q_id, result, test):
    chosen_answer = [user_a.user_answer for user_a in
                     UserAnswer.objects.filter(question_id=result['question'], test_id=test)]
    if not chosen_answer:
        answer_ids = request.POST.getlist(str(q_id))
        for ans_id in answer_ids:
            user_answer = Answer.objects.get(id=ans_id)
            choice = UserAnswer(question_id=result['question'], user_answer=user_answer, test_id=test)
            choice.save()
            chosen_answer.append(choice.user_answer)

    correct_answers = set(Answer.objects.filter(question=q_id, is_correct=True))
    is_correct = chosen_answer == correct_answers or set(chosen_answer).issubset(correct_answers)
    result.update({
        'chosen_answer': chosen_answer,
        'correct_answer': correct_answers,
        'is_correct': is_correct
    })
